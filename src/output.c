#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>

#include "keyboard.h"
#include "output.h"
#include "server.h"
#include "view.h"

void
output_frame(struct wl_listener* listener, void* data)
{
	// This is called every time an output is ready to display a frame
	// (generally at the output's refresh rate e.g. 60Hz)
	struct mwm_output* moutput = wl_container_of(listener, moutput, frame);
	struct wlr_renderer* renderer = moutput->server->renderer;

	// only ever one output, so data is the output
	struct wlr_output* output = data;

	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	// wlr_output_attach_render makes the OpenGL context current
	if (!wlr_output_attach_render(output, NULL)) {
		return;
	}

	// Begin the renderer (calls glViewport and some other GL sanity checks)
	wlr_renderer_begin(renderer, (unsigned)output->width, (unsigned)output->height);

	// RGBA
	float color[4] = {.2f, 0.0f, 0.0f, 1.0f};
	wlr_renderer_clear(renderer, color);

	struct mwm_view* view;
	wl_list_for_each_reverse(view, &moutput->server->views, link)
	{
		if (!view->mapped)
			continue;

		struct render_data rdata = {
				.output = moutput->wlr_output,
				.view = view,
				.renderer = renderer,
				.when = &now,
		};

		// Calls render_surface function for each surface among the
		// xdg_surface's toplevel and popups
		wlr_xdg_surface_for_each_surface(view->xdg_surface, render_surface, &rdata);
	}

	// Conclude rendering, swap buffers, and show the final frame on-screen
	wlr_renderer_end(renderer);
	// wlr_output_commit(output->wlr_output);
	wlr_output_commit(output);
}

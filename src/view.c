#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>

#include "keyboard.h"
#include "output.h"
#include "server.h"
#include "view.h"

void
focus_view(struct mwm_view* view, struct wlr_surface* surface)
{
	// Note: only deals with keyboard focus
	if (view == NULL)
		return;

	struct mwm_server* server = view->server;
	struct wlr_seat* seat = server->seat;
	struct wlr_surface* prev_surface = seat->keyboard_state.focused_surface;
	if (prev_surface == surface)
		// Don't re-focus an already focused surface
		return;

	if (prev_surface) {
		// Deactivate previously focused surface
		// Lets client know it no longer has focus
		// Client will repaint accordingly, e.g. stop displaying a caret
		struct wlr_xdg_surface* previous =
				wlr_xdg_surface_from_wlr_surface(seat->keyboard_state.focused_surface);
		wlr_xdg_toplevel_set_activated(previous, false);
	}
	struct wlr_keyboard* keyboard = wlr_seat_get_keyboard(seat);
	// Move the view to the front
	wl_list_remove(&view->link);
	wl_list_insert(&server->views, &view->link);
	// Activate the new surface
	wlr_xdg_toplevel_set_activated(view->xdg_surface, true);
	// Tell seat to have keyboard enter this surface
	// Wlroots will automatically send key events to appropriate clients
	wlr_seat_keyboard_notify_enter(seat, view->xdg_surface->surface, keyboard->keycodes,
			keyboard->num_keycodes, &keyboard->modifiers);
}

void
xdg_surface_map(struct wl_listener* listener, void* data)
{
	(void)data;
	// Called when the surface is mapped, or ready to display on-screen
	struct mwm_view* view = wl_container_of(listener, view, map);
	view->mapped = true;
	focus_view(view, view->xdg_surface->surface);
}

void
xdg_surface_unmap(struct wl_listener* listener, void* data)
{
	(void)data;
	// Called when the surface is unmapped, and should no longer be shown
	struct mwm_view* view = wl_container_of(listener, view, unmap);
	view->mapped = false;
}

void
xdg_surface_destroy(struct wl_listener* listener, void* data)
{
	(void)data;
	// Called when the surface is destroyed and should never be shown again
	struct mwm_view* view = wl_container_of(listener, view, destroy);
	wl_list_remove(&view->link);
	// Is this needed?
	// free(view->xdg_surface);
	free(view);
}

void
render_surface(struct wlr_surface* surface, int sx, int sy, void* data)
{
	// This is called for every surface that needs to be rendered
	struct render_data* rdata = data;
	// struct mwm_view* view = rdata->view;
	struct wlr_output* output = rdata->output;

	// We first obtain a wlr_texture, which is a GPU resource
	// Wlroots automatically handles negotiating these with the client
	// The underlying resource could be an opaque handle passed from the client
	// or the client could have sent a pixel buffer which we copied to the GPU
	// or a few other means. You don't have to worry about this, wlroots takes care of it
	struct wlr_texture* texture = wlr_surface_get_texture(surface);
	if (texture == NULL)
		return;

	// We also have to apply the scale factor (mwm does not support HiDPI)
	struct wlr_box box = {.x = sx,
			.y = sy,
			.width = surface->current.width,
			.height = surface->current.height};

	// We need to prepare a matrix to render the view with
	float matrix[9];
	enum wl_output_transform transform =
			wlr_output_transform_invert(surface->current.transform);
	// Helper which takes a box with desired x, y coords, width, height,
	// output geometry, then prepares ortho projection and multiplies transforms
	// to produce a MVP matrix
	wlr_matrix_project_box(matrix, &box, transform, 0, output->transform_matrix);

	// Takes matrix, texture, and alpha, and performs the actual rendering
	wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1);

	// Lets client know we've displayed the frame, it can prepare another one now
	wlr_surface_send_frame_done(surface, rdata->when);
}

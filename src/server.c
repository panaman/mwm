#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>

#include "keyboard.h"
#include "output.h"
#include "server.h"
#include "view.h"

bool
server_init(struct mwm_server* srv)
{
	wlr_log(WLR_INFO, "Setting up mwm server");

	srv->display = wl_display_create();
	srv->backend = wlr_backend_autocreate(srv->display);
	srv->renderer = wlr_backend_get_renderer(srv->backend);

	wlr_renderer_init_wl_display(srv->renderer, srv->display);
	// Hands-off interfaces: Compositor needed for clients
	// Data-device-manager handles clipboard
	wlr_compositor_create(srv->display, srv->renderer);
	wlr_data_device_manager_create(srv->display);

	srv->output_layout = wlr_output_layout_create();

	// Config listener notified when new outputs are available on backend
	srv->new_output.notify = server_new_output;
	wl_signal_add(&srv->backend->events.new_output, &srv->new_output);

	// Set up our list of views and the xdg-shell-> The xdg-shell is a Wayland
	// protocol which is used for application windows
	wl_list_init(&srv->views);
	srv->xdg_shell = wlr_xdg_shell_create(srv->display);

	srv->new_xdg_surface.notify = server_new_xdg_surface;
	wl_signal_add(&srv->xdg_shell->events.new_surface, &srv->new_xdg_surface);

	srv->cursor = wlr_cursor_create();
	wlr_cursor_attach_output_layout(srv->cursor, srv->output_layout);

	srv->cursor_mgr = wlr_xcursor_manager_create(NULL, 24);
	wlr_xcursor_manager_load(srv->cursor_mgr, 1);

	// configs seat: user operates computer (up to one keyboard and pointer)
	srv->seat = wlr_seat_create(srv->display, "seat0");

	srv->new_input.notify = server_new_input;
	wl_signal_add(&srv->backend->events.new_input, &srv->new_input);
	/*
	srv->request_cursor->notify = seat_request_cursor;
	wl_signal_add(&srv->seat->events->request_set_cursor,
			 &srv->request_cursor);
	 */
	const char* socket = wl_display_add_socket_auto(srv->display);
	if (!socket) {
		wlr_backend_destroy(srv->backend);
		return false;
	}

	if (!wlr_backend_start(srv->backend)) {
		wlr_backend_destroy(srv->backend);
		wl_display_destroy(srv->display);
		return false;
	}

	setenv("WAYLAND_DISPLAY", socket, true);
	return true;
}

void
server_run(struct mwm_server* srv)
{
	wlr_log(WLR_INFO, "Running mwm server");
	wl_display_run(srv->display);
}

void
server_finish(struct mwm_server* srv)
{
	wlr_log(WLR_INFO, "Shutting mwm down");

	wlr_backend_destroy(srv->backend);
	wl_display_destroy_clients(srv->display);
	wl_display_destroy(srv->display);
}

void
server_new_input(struct wl_listener* listener, void* data)
{
	// This event is raised by backend when new input device becomes available
	struct mwm_server* server = wl_container_of(listener, server, new_input);
	struct wlr_input_device* device = data;

	switch (device->type) {
		case WLR_INPUT_DEVICE_KEYBOARD:
			server_new_keyboard(server, device);
			break;
		case WLR_INPUT_DEVICE_POINTER:
			server_new_pointer(server, device);
		default:
			break;
	}
	// Let wlr_seat know our capabilities
	// In mwm, always have a cursor and a keyboard
	wlr_seat_set_capabilities(server->seat,
			WL_SEAT_CAPABILITY_POINTER | WL_SEAT_CAPABILITY_KEYBOARD);
}

void
server_new_output(struct wl_listener* listener, void* data)
{
	// This is rasied by backend when new output (display or monitor) becomes available
	struct mwm_server* server = wl_container_of(listener, server, new_output);
	struct wlr_output* wlr_output = data;

	if (!wl_list_empty(&wlr_output->modes)) {
		struct wlr_output_mode* mode = wlr_output_preferred_mode(wlr_output);
		wlr_output_set_mode(wlr_output, mode);
		wlr_output_enable(wlr_output, true);

		if (!wlr_output_commit(wlr_output))
			return;
	}

	// Allocates and configures our state for this output
	struct mwm_output* output = malloc(sizeof(struct mwm_output));
	output->wlr_output = wlr_output;
	output->server = server;
	// Sets up a listener for the frame notify event
	output->frame.notify = output_frame;
	wl_signal_add(&wlr_output->events.frame, &output->frame);

	// Adds this to output layout
	// Arranges outputs from left-to-right in the order they appear
	// A sophisticated compositor would let user config the arrangement of outputs
	wlr_output_layout_add_auto(server->output_layout, wlr_output);

	// Creating the global adds a wl_output global to the display
	// Wayland clients can see to find out information about the output
	// (DPI, scale factor, manufacturer, etc)
	wlr_output_create_global(wlr_output);
}

void
server_new_pointer(struct mwm_server* server, struct wlr_input_device* device)
{
	// Don't do anything special with pointers
	// All pointer handling is proxied through wlr_cursor
	// TODO: libinput configuration to set acceleration, etc
	wlr_cursor_attach_input_device(server->cursor, device);
}

void
server_new_keyboard(struct mwm_server* server, struct wlr_input_device* device)
{
	struct mwm_keyboard* kbd = malloc(sizeof(struct mwm_keyboard));
	kbd->server = server;
	kbd->device = device;

	// Prepare XKB keymap and assign it to keyboard
	// Assumes default layout = "us"
	struct xkb_rule_names rules = {0};
	struct xkb_context* ctx = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	struct xkb_keymap* keymap =
			xkb_map_new_from_names(ctx, &rules, XKB_KEYMAP_COMPILE_NO_FLAGS);

	wlr_keyboard_set_keymap(device->keyboard, keymap);
	xkb_keymap_unref(keymap);
	xkb_context_unref(ctx);
	wlr_keyboard_set_repeat_info(device->keyboard, 25, 600);

	// Set up listeners for keyboard events
	kbd->modifiers.notify = keyboard_handle_modifiers;
	wl_signal_add(&device->keyboard->events.modifiers, &kbd->modifiers);
	kbd->key.notify = keyboard_handle_key;
	wl_signal_add(&device->keyboard->events.key, &kbd->key);

	wlr_seat_set_keyboard(server->seat, device);
}

void
server_new_xdg_surface(struct wl_listener* listener, void* data)
{
	// Raised when wlr_xdg_shell receives a new xdg surface from a
	// client: toplevel (application window) or popup
	struct mwm_server* server = wl_container_of(listener, server, new_xdg_surface);
	struct wlr_xdg_surface* xdg_surface = data;

	if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL)
		return;

	// Allocate a mwm_view for this surface
	struct mwm_view* view = malloc(sizeof(struct mwm_view));
	view->server = server;
	view->xdg_surface = xdg_surface;

	// Listen to the various events it can emit
	view->map.notify = xdg_surface_map;
	wl_signal_add(&xdg_surface->events.map, &view->map);
	view->unmap.notify = xdg_surface_unmap;
	wl_signal_add(&xdg_surface->events.unmap, &view->unmap);
	view->destroy.notify = xdg_surface_destroy;
	wl_signal_add(&xdg_surface->events.destroy, &view->destroy);

/*
	struct wlr_xdg_toplevel* toplevel = xdg_surface->toplevel;
	view->request_move.notify = xdg_toplevel_request_move;
	wl_signal_add(&toplevel->events.request_move, &view->request_move);
	view->request_resize.notify = xdg_toplevel_request_resize;
	wl_signal_add(&toplevel->events.request_resize, &view->request_resize);
*/
	// Add it to the list of views
	wl_list_insert(&server->views, &view->link);
}

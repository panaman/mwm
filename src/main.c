#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/util/log.h>

#include "keyboard.h"
#include "output.h"
#include "server.h"
#include "view.h"

// clang-format off
static const struct option longopts[] = {
		{"lmao", no_argument, 0, 'l'},
		{"version", no_argument, 0, 'v'},
		{"help", no_argument, 0, 'h'},
		{0, 0, 0, 0}
};
// clang-format on

static const char* const usage =
		"Usage:\n"
		"	mwm [OPTIONS]\n\n"
		"OPTIONS:\n"
		"	-l, --lmao        DNE\n"
		"	-h, --help        Show help message and quit.\n"
		"	    --version     Show the version number and quit.\n";

int
main(int argc, char** argv)
{
	int c;
	while ((c = getopt_long(argc, argv, "vlh", longopts, NULL)) != -1) {
		switch (c) {
			case 'l':
				puts("[INSERT JOKE HERE]");
				return 0;
			case 'v':
				puts("mwm version " MWM_VERSION);
				return 0;
			case 'h':
				puts(usage);
				return 0;
			default:
				puts(usage);
				return 1;
		}
	}

	wlr_log_init(WLR_DEBUG, NULL);

	// All fields guaranteed valid before startup, no need to zero-out
	struct mwm_server server;

	if (!server_init(&server)) {
		// TODO
	}

	// Start the visual compositor
	// Returns when session ends
	wl_display_run(server.display);

	wlr_log(WLR_ERROR, "Session ended");

	wl_display_destroy_clients(server.display);
	wl_display_destroy(server.display);

	return 0;
}

#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>

#include "keyboard.h"
#include "output.h"
#include "server.h"
#include "view.h"

bool
handle_keybinding(struct mwm_server* server, xkb_keysym_t sym)
{
	// Handle compositor keybindings (assumes Alt is held)
	switch (sym) {
		case XKB_KEY_Escape:
			wl_display_terminate(server->display);
			break;
		default:
			return false;
	}
	return true;
}

void
keyboard_handle_modifiers(struct wl_listener* listener, void* data)
{
	(void)data;
	// This event raised when a modifier, shift or alt, pressed
	// We communicate this to the client
	struct mwm_keyboard* kbd = wl_container_of(listener, kbd, modifiers);

	// A seat can only have one keyboard, mwm never has more

	// Send modifiers to client
	wlr_seat_keyboard_notify_modifiers(kbd->server->seat,
			&kbd->device->keyboard->modifiers);
}

void
keyboard_handle_key(struct wl_listener* listener, void* data)
{
	struct mwm_keyboard* kbd = wl_container_of(listener, kbd, key);
	struct mwm_server* server = kbd->server;
	struct wlr_seat* seat = server->seat;

	const struct wlr_event_keyboard_key* event = data;

	// libinput keycode -> xkbcommon
	unsigned keycode = event->keycode + 8;
	// keysym list based on keyboard keymap
	const xkb_keysym_t* syms;
	const int nsyms =
			xkb_state_key_get_syms(kbd->device->keyboard->xkb_state, keycode, &syms);

	bool handled = false;
	unsigned modifiers = wlr_keyboard_get_modifiers(kbd->device->keyboard);
	// TODO: CTRL -> LOGO
	if ((modifiers & WLR_MODIFIER_ALT) &&
			event->state == WL_KEYBOARD_KEY_STATE_RELEASED) {
		// if mod + this button, process as compositor keybinding
		for (int i = 0; i < nsyms; i++) {
			// TODO: enable handled and forwarding to client
			handled = handle_keybinding(server, syms[i]);
			// handle_keybinding(server, syms[i]);
		}
	}

	if (!handled) {
		// Otherwise, we pass it along to the client
		wlr_seat_set_keyboard(seat, kbd->device);
		wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode,
				event->state);
	}
}

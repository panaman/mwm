#pragma once

#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>

struct mwm_server {
	struct wl_display* display;
	struct wlr_backend* backend;
	struct wlr_renderer* renderer;
	struct wlr_xdg_shell* xdg_shell;
	struct wlr_output_layout* output_layout;
	struct wlr_seat* seat;
	struct wlr_cursor* cursor;
	struct wlr_xcursor_manager* cursor_mgr;

	struct wl_list views;

	struct wl_listener new_xdg_surface;
	struct wl_listener output_frame;
	struct wl_listener cursor_motion;
	struct wl_listener cursor_button;
	struct wl_listener cursor_axis;
	struct wl_listener cursor_frame;
	struct wl_listener request_cursor;
	struct wl_listener new_input;
	struct wl_listener new_output;
};

bool server_init(struct mwm_server*);
void server_run(struct mwm_server*);
void server_finish(struct mwm_server*);

void server_new_input(struct wl_listener*, void*);
void server_new_output(struct wl_listener*, void*);
void server_new_pointer(struct mwm_server*, struct wlr_input_device*);
void server_new_keyboard(struct mwm_server*, struct wlr_input_device*);
void server_new_xdg_surface(struct wl_listener*, void*);

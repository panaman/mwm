#pragma once

#include <wayland-server-core.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>

#include "server.h"

struct mwm_keyboard {
	struct mwm_server* server;
	struct wlr_input_device* device;

	struct wl_listener modifiers;
	struct wl_listener key;
};

bool handle_keybinding(struct mwm_server*, xkb_keysym_t);
void keyboard_handle_modifiers(struct wl_listener*, void*);
void keyboard_handle_key(struct wl_listener*, void*);

#pragma once

#include <wayland-server-core.h>
#include <wlr/types/wlr_output.h>

#include "keyboard.h"
#include "server.h"

struct mwm_output {
	struct mwm_server* server;
	struct wlr_output* wlr_output;

	struct wl_listener frame;
	struct wl_listener destroy;
};

void output_frame(struct wl_listener*, void*);

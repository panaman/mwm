#pragma once

#include <wayland-server-core.h>
#include <wlr/types/wlr_xdg_shell.h>

#include "keyboard.h"
#include "server.h"

enum mwm_view_prop {
	VIEW_PROP_TITLE,
	VIEW_PROP_APP_ID,
	VIEW_PROP_CLASS,
	VIEW_PROP_INSTANCE,
	VIEW_PROP_WINDOW_TYPE,
	VIEW_PROP_WINDOW_ROLE,
};

struct mwm_view {
	struct wl_list link;

	struct mwm_server* server;
	struct wlr_xdg_surface* xdg_surface;

	struct wl_listener map;
	struct wl_listener unmap;
	struct wl_listener destroy;

	bool mapped;
};

// Used to move all data needed to render surface from the top-level
// frame handler to the per-surface render function
struct render_data {
	struct wlr_output* output;
	struct wlr_renderer* renderer;
	struct mwm_view* view;
	struct timespec* when;
};

void focus_view(struct mwm_view*, struct wlr_surface*);
void xdg_surface_map(struct wl_listener*, void*);
void xdg_surface_unmap(struct wl_listener*, void*);
void xdg_surface_destroy(struct wl_listener*, void*);
void render_surface(struct wlr_surface*, int, int, void*);
